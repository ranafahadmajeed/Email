��    
      l      �       �      �      �        
   (     3     S     p     }     �  �  �     y     �  $   �     �  +   �  /   �     '     <     B                                      	   
    Already sent Files Attached Select Student email field Send Email Send Email to Selected Students Send Email to Selected Users Send copy to Subject [Copy] Project-Id-Version: Email module
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-03-13 16:15+0100
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 2.2.1
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Déjà envoyé Fichiers Joints Sélectionner le champ email Élève Envoyer Email Envoyer l'Email aux Élèves Sélectionnés Envoyer l'Email aux Utilisateurs Sélectionnés Envoyer une copie à Sujet [Copie] 