Email module
============

![screenshot](https://gitlab.com/francoisjacquet/Email/raw/master/screenshot.png?inline=false)

https://www.rosariosis.org/modules/email/

Version 6.1 - October, 2021

License GNU GPL v2

Author François Jacquet

Sponsored by Microtec Guatemala & English National Program

DESCRIPTION
-----------
This additional module extends the following modules:

- Students: Send Email.
- Users: Send Email.

The email body is customizable.

Attach multiple files (up to 24MB).

(Students) Send a copy to administrators and teachers.

A test email can be sent before sending to students or users.

Translated in [French](https://www.rosariosis.org/fr/modules/email/) and [Spanish](https://www.rosariosis.org/es/modules/email/).

CONTENT
-------
Students
- Send Emails

Users
- Send Emails

INSTALL
-------
Copy the `Email/` folder (if named `Email-master`, rename it) and its content inside the `modules/` folder of RosarioSIS.

Go to _School > Configuration > Modules_ and click "Activate".

Requires RosarioSIS 5.2+
